FROM debian:buster-slim AS builder
LABEL stage=builder

ENV MGOD_VERSION 1.1

WORKDIR /usr/local/src/mgod

RUN apt update; apt install --yes build-essential ctags wget
RUN wget -O mgod.tar.gz https://port70.net/?9mgod/mgod-${MGOD_VERSION}.tar.gz
RUN tar xzf mgod.tar.gz; mv mgod-${MGOD_VERSION}/* . ; rmdir mgod-${MGOD_VERSION}
RUN make

FROM debian:buster-slim

ARG hostname="127.0.0.1"

COPY --from=builder /usr/local/src/mgod/mgod /usr/local/sbin/mgod

RUN apt update; apt install --yes openbsd-inetd
RUN useradd -s /usr/sbin/nologin _gopher; mkdir /var/gopher; chown _gopher /var/gopher
RUN echo "gopher stream tcp nowait _gopher /usr/local/sbin/mgod mgod -r /var/gopher -n ${hostname}" >> /etc/inetd.conf;

EXPOSE 70

CMD ["inetd", "-d"]
