# docker-mgod
A dockerized version of the [mgod gopher server](https://port70.net/?1mgod).

## Installation
```
git clone https://gitlab.com/sxvghd/docker-mgod
cd docker-mgod
docker build -t mgod --build-arg hostname="missisipi.com" .
```
The `hostname` buildarg is used by mgod for the creation of absolute URIs.
It's optional, defaulting to `127.0.0.1`.

## Usage
Ex. Creating a container with a direct port mapping and a content bind
```
docker create --name gopher1 -p 70:70 --mount type=bind,source=/var/goph1,target=/var/gopher mgod
```
As seen above, `/var/gopher` is the content folder on the target container.

